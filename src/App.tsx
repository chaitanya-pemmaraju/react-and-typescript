import * as React from 'react';
import './App.css';

import logo from './logo.svg';

class App extends React.Component {
  public render() {
    const sampleInfo:Isample = {
      age : 32,
      name : "srihari"   
    }
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Hello this is purely for testing purposes to demo {sampleInfo.name}</h1>
        </header>
        <p className="App-intro">
          This is test app <b> this is my first app in react and typescript</b>
		  lets try some more.
        </p>
		<p>Lets try some real Tasks</p>
      </div>
    );
  }
}

export interface Isample {
  name:string,
  age:number
}

export default App;
